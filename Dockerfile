FROM node:6.9.0
RUN mkdir /app
WORKDIR /app
COPY package.json /app
RUN npm install
RUN npm install pm2 -g
COPY . /app
EXPOSE 3000
CMD pm2 start app.js && pm2 logs
